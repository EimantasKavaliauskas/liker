import { Component, OnInit } from '@angular/core';
import {SuggestionModel} from './models/suggestion-model';
import {Guid} from '../../services/guid-generator';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  dataSource: SuggestionModel[];
  newSuggestion = '';

  constructor() { }

  ngOnInit() {
    this.dataSource = [];
    this.dataSource.push({id: Guid.newGuid(), suggestion: 'Item 1', likes: 1, likedByUser: false});
    this.dataSource.push({id: Guid.newGuid(), suggestion: 'Item 2', likes: 5, likedByUser: true});
    this.dataSource.push({id: Guid.newGuid(), suggestion: 'Item 3', likes: 1, likedByUser: false});
    this.dataSource.push({id: Guid.newGuid(), suggestion: 'Item 4', likes: 0, likedByUser: false});
  }

  onEnterClick(event) {
    if (event.keyCode === 13) {
      this.onAddClick();
    }
  }

  onAddClick() {
    if (this.newSuggestion === '') {
      return;
    }
    this.dataSource.push({id: Guid.newGuid(), suggestion: this.newSuggestion, likes: 0, likedByUser: false});
    this.newSuggestion = '';
  }

  onLikeClick(element) {
    const item = this.dataSource.find(e => e.id === element.id);
    if (item.likedByUser) {
      item.likes--;
    }
    else {
      item.likes++;
    }
    item.likedByUser = !item.likedByUser;
  }
}
