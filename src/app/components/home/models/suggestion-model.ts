
export interface SuggestionModel {
  id: string;
  suggestion: string;
  likes: number;
  likedByUser: boolean;
}
